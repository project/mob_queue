<?php

namespace Drupal\mob_queue\Commands;

use Drupal\Component\Utility\Environment;
use Drupal\Core\Session\AccountSwitcherInterface;
use Drupal\Core\Session\AnonymousUserSession;
use Drupal\mob_queue\QueueJobOperator;
use Drush\Commands\DrushCommands;

/**
 * The Drush command file for mob_queue.
 */
class MobQueueCommands extends DrushCommands {

  /**
   * Account switcher.
   *
   * @var \Drupal\Core\Session\AccountSwitcherInterface
   */
  protected $accountSwitcher;

  /**
   * Mob Queue Operator.
   *
   * @var \Drupal\mob_queue\QueueJobOperator
   */
  protected $mobQueueOperator;

  /**
   * MobQueueCommands constructor.
   *
   * @param \Drupal\Core\Session\AccountSwitcherInterface $accountSwitcher
   *   Account Switcher.
   * @param \Drupal\mob_queue\QueueJobOperator $mobQueueOperator
   *   Mob Queue Operator.
   */
  public function __construct(AccountSwitcherInterface $accountSwitcher, QueueJobOperator $mobQueueOperator) {
    $this->accountSwitcher = $accountSwitcher;
    $this->mobQueueOperator = $mobQueueOperator;
  }

  /**
   * Execute mob_queue queued tasks.
   *
   * @param $time
   *   Total execution time for this command.
   * @param array $options
   *   An associative array of options whose values come from cli, aliases,
   *   config, etc.
   *
   * @option no-reset-expired
   *   Do not reset expired items from the queue table.
   * @usage drush mob-queue
   *   Go a sprint to finish the tasks in mob_queue queues.
   *
   * @command mob:exe-queue
   * @aliases meq,mob-exe-queue
   */
  public function exeQueue($time, array $options = ['no-reset-expired' => NULL]) {
    // Allow execution to continue even if the request gets canceled.
    @ignore_user_abort(TRUE);

    if (!$options['no-reset-expired']) {
      // Reset expired items in the default queue implementation table.
      $updated = \Drupal::database()->update('queue')->fields([
        'expire' => 0,
      ])
        ->condition('expire', 0, '<>')
        ->condition('expire', \Drupal::time()->getRequestTime(), '<')
        ->execute();
      $this->logger()->warning(dt('!updated expired items reset.', ['!updated' => $updated]));
    }

    // Force the current user to anonymous to ensure consistent permissions on
    // cron runs.
    $this->accountSwitcher->switchTo(new AnonymousUserSession());

    // Try to allocate enough time to run all the hook_cron implementations.
    Environment::setTimeLimit($time);

    // Grab the defined cron queues.
    $queues = $this->mobQueueOperator->getQueueJobs();
    \Drupal::moduleHandler()->alter('mob_queue_cron_queue_info', $queues);

    reset($queues);
    while (TRUE) {
      // Due to backwards incompatible changes on array handling from PHP 5.x to
      // 7.x, looping code must be kept simple. For instance, current item
      // pointer should be advanced manually, since foreach() does not starting
      // from 7.0.
      // @see #2974823 for more info.
      $queue_name = key($queues);
      $info = current($queues);
      if ($queue_name === NULL && $info === FALSE) {
        break;
      }
      next($queues);

      // Ensure the time is passed to the info variable.
      $info['mob_queue']['time'] = $time;

      // Allow other modules to alter the queues listing, order queue to
      // process.
      \Drupal::moduleHandler()->alter('mob_queue_queue_processing', $queue_name, $info, $queues);
      $this->mobQueueOperator->processQueues($queue_name, $info);
      \Drupal::moduleHandler()->invokeAll('mob_queue_queue_processed', [
        $queue_name,
        $info,
        $queues,
      ]);
    }

    // Restore the user.
    $this->accountSwitcher->switchBack();
  }

}
